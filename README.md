# DevOps with GitLab CI - Build Pipelines and Deploy to AWS

## Objectifs

- Démarrer un projet ReactJS en local avec Docker
- Le déployer en ligne avec GitLab CI

## Prérequis

- docker desktop installé (https://www.docker.com/products/docker-desktop/) ou docker & docker compose (v2)
- (windows) avoir installé npm sur son ordi (https://radixweb.com/blog/installing-npm-and-nodejs-on-windows-and-mac)
- (Linux) git (sudo apt update && sudo apt install git)
- (Windows) git (https://git-scm.com/download/win)
- un éditeur de code (VS Code ou autre)
- être connecté au dépôt Git (échange clé ssh par exemple) :
  - sur le poste local, effectuer la commande suivante : ssh-keygen
  - lorsque la commande demande où sera stocké la clé, cliquer sur Entrée ou définir le chemin
  - définir le passphrase. La clé sera générée
  - Accéder au fichier .pub et copier le contenu sur le compte gitlab (Edition du profil, SSH Keys, ajouter une clé)

## Installation du projet

- Ouvrir un terminal et cloner le projet : `git clone git@gitlab.com:DaniSoudry99/gitlab-ci-react-ms2d.git`
- Entrer dans le répertoire du projet
- Démarrer Docker Desktop
- Entrer la commande : `docker-compose up -d`
- Attendre la fin de l'éxecution de la commande
- Accéder au projet sur votre navigateur : (http://localhost:5000)

## Pour faire des modifications dans le code

- Créez vous votre propre branche
  - `git checkout -b prenom.nom`
